package com.nlds.javaexp;

import java.util.*;

/*
Write a Java program to divide a string in n equal parts.
The given string is: abcdefghijklmnopqrstuvwxy
The string divided into 5 parts and they are: 

abcde
fghij
klmno
pqrst
uvwxy
*/

public class TestStringSep {
	
	public static ArrayList<String> splitString(String str1, int n) {

		ArrayList<String> strings = new ArrayList<String>();
		int charByPart = str1.length() / n;
		char[] chars = str1.toCharArray();
		int count = 0;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < chars.length; i++) {
			count++;
			sb.append(chars[i]);

			if (count == charByPart) {
				System.out.println(sb.toString());
				strings.add(sb.toString());
				sb.setLength(0);
				count = 0;
			}
		}
		
		if (sb.length() > 0)
			System.out.println("It's not possible split the string, left:" + (sb.toString()));
		return strings;

	}
 
	public static void main(String[] args) {
		String str = "abcdefghijklmnopqrstuvwxy"; // length 25
		int split_number = 5;
		splitString(str, split_number);

		str = "abcfdefghijkl"; // length 13
		split_number = 3;
		splitString(str, split_number);

		str = "abcfdefghi"; // length 10
		split_number = 2;
		splitString(str, split_number);

	}
 
}