package jexperi;
import java.util.ArrayList;
import org.junit.Test;
import com.nlds.javaexp.TestStringSep;

public class TestStr {
	@Test
	public void test_5_parts() {
		String str1 = "abcdefghijklmnopqrstuvwxy";
		int parts = 5;
		ArrayList<String> resul = TestStringSep.splitString(str1, parts);
		assert (resul.get(0).equals("abcde"));
		assert (resul.get(resul.size() - 1).equals("uvwxy"));
	}

	@Test
	public void test_2parts() {
		String str1 = "abcfdefghi";
		int parts = 2;
		ArrayList<String> resul = TestStringSep.splitString(str1, parts);
		assert (resul.get(0).equals("abcfd"));
		assert (resul.get(resul.size() - 1).equals("efghi"));
	}
	@Test
	public void test_2partserr() {
		String str1 = "abcfdefghia";
		int parts = 2;
		ArrayList<String> resul = TestStringSep.splitString(str1, parts);
		assert (resul.get(0).equals("abcfd"));
		assert (resul.get(resul.size() - 1).equals("efghi"));
	}

}
